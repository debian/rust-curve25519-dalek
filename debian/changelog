rust-curve25519-dalek (4.1.3+20240618+dfsg-9) unstable; urgency=medium

  * declare rust-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + simplify usage comments
  * update watch file:
    + improve filename mangling
    + use Github API; track releases (not git commits)
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 09 Feb 2025 03:22:15 +0100

rust-curve25519-dalek (4.1.3+20240618+dfsg-8) unstable; urgency=medium

  * add patch 2001 to avoid crate merlin;
    update TODO note;
    closes: bug#1081900,
    thanks to Jeremy Bícha and (again) Blair Noctis

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 04 Oct 2024 19:52:14 +0200

rust-curve25519-dalek (4.1.3+20240618+dfsg-7) unstable; urgency=medium

  * stop mention dh-cargo in long description
  * stop provide ed25519-dalek features batch merlin
    (require separate binary package if reintroduced);
    stop (build-)depend on package for crate merlin;
    closes: bug#1081176, thanks to Blair Noctis

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 10 Sep 2024 09:31:45 +0200

rust-curve25519-dalek (4.1.3+20240618+dfsg-6) unstable; urgency=medium

  * tighten build rules to catch failure in loop
  * provide and autopkgtest ed25519-dalek features batch merlin;
    build-depend on package for crate merlin;
    drop patch 2001_merlin

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 10 Aug 2024 20:18:24 +0200

rust-curve25519-dalek (4.1.3+20240618+dfsg-5) unstable; urgency=medium

  * add patches cherry-picked upstream
    to improve mitigation of timing-based attacks;
    tighten build-dependency for crate subtle

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 30 Jul 2024 23:09:41 +0200

rust-curve25519-dalek (4.1.3+20240618+dfsg-4) unstable; urgency=medium

  * tighten substvars:
    use ${rust:Version:*} with full package name
    (see bug#1076434)
  * autopkgtest-depend on dh-rust (not dh-cargo)
  * reduce autopkgtest to check single-feature tests only on amd64

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 30 Jul 2024 17:53:12 +0200

rust-curve25519-dalek (4.1.3+20240618+dfsg-3) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 12:48:58 +0200

rust-curve25519-dalek (4.1.3+20240618+dfsg-2) unstable; urgency=medium

  * tighten package version resolving and mangling
  * update dh-cargo fork
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 10 Jul 2024 12:56:39 +0200

rust-curve25519-dalek (4.1.3+20240618+dfsg-1) unstable; urgency=high

  [ upstream ]
  * new release;
    fixes RUSTSEC-2024-0344;
    closes: bug#1074351, thanks to Salvatore Bonaccorso

  [ Jonas Smedegaard ]
  * rename patch 1002, and update DEP-3 headers
  * update watch file: bump base version
  * set urgency=high due to security bugfix
  * bump project versions in virtual packages and autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 27 Jun 2024 22:49:57 +0200

rust-curve25519-dalek (4+20240603+dfsg-2) unstable; urgency=medium

  * add patch 2003 to avoid benches;
    relax build- and autopkgtest-dependency for crate criterion

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 09 Jun 2024 00:00:56 +0200

rust-curve25519-dalek (4+20240603+dfsg-1) unstable; urgency=medium

  [ upstream ]
  * git development snapshot

  [ Jonas Smedegaard ]
  * add patch 1002 to check tests only with required features enabled;
    skip autopkgtests for features apparently unusable alone;
    together, closes: bug#1071546, thanks to Jeremy Bícha

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 07 Jun 2024 14:59:53 +0200

rust-curve25519-dalek (4+20240207+dfsg-3) unstable; urgency=medium

  * add patch 2002 to include large test assets

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 17 May 2024 16:58:03 +0200

rust-curve25519-dalek (4+20240207+dfsg-2) unstable; urgency=medium

  * simplify same-source dependencies
  * update dh-cargo fork
  * provide and autopkgtest ed25519-dalek features
    alloc asm fast zeroize;
    provide and autopkgtest x25519-dalek features
    alloc precomputed-tables;
    closes: bug#1071150, thanks to Matthias Geiger
  * update copyright info:
    + fix file path
    + update coverage
  * declare compliance with Debian Policy 4.7.0

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 16 May 2024 11:12:50 +0200

rust-curve25519-dalek (4+20240207+dfsg-1) experimental; urgency=medium

  * drop patch 2001_group,
    obsoleted by Debian package changes;
    (build-)depend on packages for crates ff group;
    provide and autopkgtest curve25519-dalek features group group-bits
  * update patches;
    update DEP-3 headers
  * fix TODO note
  * update copyright info:
    + drop superfluous sections containing suspicious FIXMEs;
      thanks to Thorsten Alteholz
  * drop patch 2001_quote,
    obsoleted by Debian package changes
  * bump project versions in virtual packages and autopkgtests
  * add patch 1001 to accept newer branch of crate toml

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 12 Feb 2024 17:56:50 +0100

rust-curve25519-dalek (4+20231122+dfsg-2) experimental; urgency=medium

  * depend (not autopkg-depend) on packages for crates platforms rustc_version

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 25 Nov 2023 01:51:33 +0100

rust-curve25519-dalek (4+20231122+dfsg-1) experimental; urgency=medium

  * initial packaging release;
    closes: bug#1052045

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 24 Nov 2023 12:29:03 +0100
